package org.bigtech.main;

import org.bigtech.conf.HelloWorldConfiguration;
import org.bigtech.rest.health.AppHealthCheck;
import org.bigtech.rest.resource.ContentNegotiationResource;
import org.bigtech.rest.resource.GithubUserResource;
import org.bigtech.rest.resource.HelloWorldResource;
import org.bigtech.rest.resource.UserResource;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Main app which starts container and REST services
 * @author Thirupathi Reddy Guduru
 * @param <T>
 * @modified Jan 18, 2015
 */
public class MainApp extends Application<HelloWorldConfiguration> {
    public static void main(final String[] args) throws Exception {
        new MainApp().run(args);
    }

    @Override
    public String getName() {
        return "hello-world";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final Bootstrap<HelloWorldConfiguration> arg0) {
        // TODO Auto-generated method stub

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run(final HelloWorldConfiguration configuration, final Environment environment) throws Exception {

        // Enable CORS headers
        final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        // DO NOT pass a preflight request to down-stream auth filters
        // unauthenticated preflight requests should be permitted by spec
        cors.setInitParameter(CrossOriginFilter.CHAIN_PREFLIGHT_PARAM, Boolean.FALSE.toString());

        final HelloWorldResource helloWorldResource = new HelloWorldResource(configuration.getTemplate(),
                configuration.getDefaultName());
        final ContentNegotiationResource negotiationResource = new ContentNegotiationResource();
        environment.jersey().register(helloWorldResource);
        environment.jersey().register(negotiationResource);
        environment.jersey().register(new GithubUserResource());
        environment.jersey().register(new UserResource());
        final AppHealthCheck appHealthCheck = new AppHealthCheck(configuration.getTemplate());
        environment.healthChecks().register("template", appHealthCheck);
    }

}
