package org.bigtech.rest.model;

/**
 * @author Guduru, Thirupathi Reddy
 *         created 11/16/17
 */
public class ContentNegotiateUser {
    private long userId;
    private String name;
    private String contentType;

    public String getContentType() {
        return this.contentType;
    }

    public ContentNegotiateUser setContentType(final String contentType) {
        this.contentType = contentType;
        return this;
    }

    public ContentNegotiateUser() {
    }

    public long getUserId() {
        return this.userId;
    }

    public ContentNegotiateUser setUserId(final long userId) {
        this.userId = userId;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public ContentNegotiateUser setName(final String name) {
        this.name = name;
        return this;
    }
}
