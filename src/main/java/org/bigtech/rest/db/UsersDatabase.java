package org.bigtech.rest.db;

import org.bigtech.rest.model.Address;
import org.bigtech.rest.model.User;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class UsersDatabase {
    private static UsersDatabase ourInstance = new UsersDatabase();

    private Map<Long, User> userMap = new HashMap<>();

    public static UsersDatabase getInstance() {
        return ourInstance;
    }

    private UsersDatabase() {
        for (int i = 0; i < 5; i++) {
            Address address = new Address();
            address.setId(1);
            address.setSteet("101 Main Street");
            address.setCity("OP");
            address.setState("MO");
            address.setZip("112233");
            User user = new User();
            user.setId(1);
            user.setName("Bob Smith : " + i);
            user.setPhone("112-334-5566");
            user.setEmail("email@email.com");
            user.setAddress(address);
            userMap.put((long) i, user);
        }
    }

    public Collection<User> getAllUsers() {
        return userMap.values();
    }

    public User getUserById(long id) {
        return userMap.get(id);
    }

    public void addUser(User user) {
        userMap.put(user.getId(), user);
    }
}
