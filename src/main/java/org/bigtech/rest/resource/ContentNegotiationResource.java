package org.bigtech.rest.resource;

import org.bigtech.rest.model.ContentNegotiateUser;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * A resource which implements API versions using content negotiation
 * @author Guduru, Thirupathi Reddy
 *         created 5/5/17
 */
@Path("/content")
public class ContentNegotiationResource {

    @GET
    @Consumes("application/json+v1")
    @Produces(MediaType.TEXT_PLAIN)
    public String getV1Content() {
        return "V1 Content";
    }

    @GET
    @Consumes({ "application/json+v2" })
    @Produces(MediaType.TEXT_PLAIN)
    public String getV2Content() {
        return "V2 Content";
    }

    @POST
    @Consumes("application/contentNegotiateUser.v1.json")
    @Produces(MediaType.APPLICATION_JSON)
    public ContentNegotiateUser saveUserV1(final ContentNegotiateUser contentNegotiateUser) {
        System.out.println("V1 ContentNegotiateUser Created");
        contentNegotiateUser.setContentType("v1");
        return contentNegotiateUser;
    }

    @POST
    @Consumes("application/contentNegotiateUser.v2.json")
    @Produces(MediaType.APPLICATION_JSON)
    public ContentNegotiateUser saveUserV2(final ContentNegotiateUser contentNegotiateUser) {
        System.out.println("V2 ContentNegotiateUser Created");
        contentNegotiateUser.setContentType("v2");
        return contentNegotiateUser;
    }
}
