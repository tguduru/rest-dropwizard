package org.bigtech.rest.resource;

import org.bigtech.rest.db.UsersDatabase;
import org.bigtech.rest.model.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

/**
 * @author Guduru, Thirupathi Reddy
 * @modified: 2019-08-19
 */
@Path("/users")
public class UserResource {
    private UsersDatabase usersDatabase = UsersDatabase.getInstance();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<User> getUser() {
        return usersDatabase.getAllUsers();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUserById(@PathParam("id") long id) {
        return usersDatabase.getUserById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(User user) {
        usersDatabase.addUser(user);
        return Response.ok().build();
    }
}
