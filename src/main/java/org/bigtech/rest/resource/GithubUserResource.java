package org.bigtech.rest.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.io.IOException;

/**
 * @author Guduru, Thirupathi Reddy
 *         created 7/18/17
 */
@Path("/github")
public class GithubUserResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{user}")
    public String getUserDetails(@PathParam("user") final String user) throws IOException {
        /*
         * final CloseableHttpClient httpClient = HttpClients.createDefault();
         * final HttpGet httpGet = new HttpGet(config.getURI() + user);
         * final CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
         * final HttpEntity httpEntity = httpResponse.getEntity();
         * if (httpEntity != null) {
         * return new BufferedReader(new InputStreamReader(httpEntity.getContent())).lines().parallel()
         * .collect(Collectors.joining("\n "));
         * }
         */
        return null;
    }
}
